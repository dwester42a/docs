---
title: Extension points for the admin console
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
date: "2016-11-01"
---

{{< reuse-page path="docs/content/cloud/jira/platform/extension-points-for-the-admin-console.md">}}
---
title: Context parameters
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
date: "2016-10-31"
---
{{< include path="docs/content/cloud/connect/concepts/jira-context-parameters.snippet.md">}}

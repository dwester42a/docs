---
title: Boards 
platform: cloud
product: jswcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-software-modules-boards-39990330.html
- /jiracloud/jira-software-modules-boards-39990330.md
confluence_id: 39990330
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990330
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990330
date: "2016-09-15"
---
# Boards

This pages lists the JIRA Software modules for boards. 


## Board area

Adds a dropdown menu to a board, next to the Boards menu.

#### Module type
`webSection` + `webPanel`

#### Location key
`jira.agile.board.tools`

#### Screenshot
<img src="../images/jsw-board-menu.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "board-links",
            "location": "jira.agile.board.tools",
            "weight": 10,
            "name": {
                "value": "My Extension"
            }
        }
    ],   
    "webPanels": [
        {
            "key": "my-web-panel",
            "url": "web-panel?id={board.id}&mode={board.screen}",
            "location": "board-links",
            "name": {
                "value": "My Web Panel"
            },
            "layout": {
                "width": "100px",
                "height": "100px"
            }
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: For the webSection, set the location to `jira.agile.board.tools`. For each webPanel, set the location to the key of the webSection.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  Target URL of the menu item.

`layout`

-   **Type**: string 
-   **Required**: yes
-   **Description**: Set the 'height' and 'width' in pixels, by using an integer with 'px' appended.

----

## Board configuration

Adds a board configuration page.

#### Module type
`webPanel`

#### Screenshot
<img src="../images/jswdev-boardconfigpage.png"/> 

#### Sample JSON
``` json
...
"modules": {       
    "webPanels": [
        {
            "key": "my-configuration-page",
            "url": "configuration?id={board.id}&type={board.type}",
            "location": "jira.agile.board.configuration",
            "name": {
                "value": "My board configuration page"
            },
            "weight": 10
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the add-on resource that provides the content. This URL must be relative to the add-on's base URL. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

`location`

-   **Description**: Set the location to `jira.agile.board.configuration`.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.


  [i18n property]: /cloud/jira/platform/connect/modules/i18n-property
  [additional context]: /cloud/jira/software/context-parameters
    
---
title: JIRA Cloud Platform REST API
platform: cloud
product: jiracloud
category: reference
subcategory: api 
aliases:
- /jiracloud/jira-cloud-platform-rest-api-39987036.html
- /jiracloud/jira-cloud-platform-rest-api-39987036.md
confluence_id: 39987036
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987036
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987036
date: "2016-09-14"
---
# JIRA Cloud platform REST API

The JIRA REST APIs are used to interact with the JIRA Cloud applications remotely, for example, when building Connect add-ons or configuring webhooks. The JIRA Cloud platform provides a REST API for common features, like issues and workflows. Read the reference documentation to get started:

[JIRA Cloud platform REST API]

If you haven't used the JIRA REST APIs before, make sure you read the [Atlassian REST API policy].

## REST APIs for the JIRA applications

The JIRA Software and JIRA Service Desk applications also have REST APIs for their application-specific features, like sprints (JIRA Software) or customer requests (JIRA Service Desk).

-   [JIRA Software Cloud REST API]
-   [JIRA Service Desk Cloud REST API]

## Authentication and authorization

-	Authentication --- If you are building an Atlassian Connect add-on, [authentication (JWT-based)] is handled by the Atlassian Connect libraries. If you are calling the REST APIs directly, the following authentication methods are supported: [OAuth 1.0a], [basic authentication], [cookie-based authentication].

-	Authorization --- If you are building an Atlassian Connect add-on, authorization is handled by [scopes] and add-on users, or by [exchanging a JWT for an OAuth 2.0 access token]. If you are calling the REST APIs directly, authorization is based on the user used in the authentication process.

For more information on authentication and authorization, read the [Security overview].

  [Atlassian REST API policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [JIRA Cloud platform REST API]: https://docs.atlassian.com/jira/REST/cloud/
  [JIRA Software Cloud REST API]: https://docs.atlassian.com/jira-software/REST/cloud/
  [JIRA Service Desk Cloud REST API]: https://docs.atlassian.com/jira-servicedesk/REST/cloud/
  [scopes]: /cloud/jira/platform/jira-rest-api-scopes
  [authentication (JWT-based)]: /cloud/jira/platform/authentication-for-add-ons/
  [basic authentication]: /cloud/jira/platform/jira-rest-api-basic-authentication
  [cookie-based authentication]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication
  [OAuth 1.0a]: /cloud/jira/platform/jira-rest-api-oauth-authentication
  [exchanging a JWT for an OAuth 2.0 access token]: /cloud/jira/platform/oauth-2-jwt-bearer-token-authorization-grant-type  
  [Security overview]: /cloud/jira/platform/security-overview

---
title: Extension points for the View issue page
platform: cloud
product: jiracloud
category: reference
subcategory: modules 
aliases:
- /jiracloud/jira-platform-modules-view-issue-page-39988380.html
- /jiracloud/jira-platform-modules-view-issue-page-39988380.md
confluence_id: 39988380
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988380
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988380
date: "2016-09-30"
---
# Extension points for the 'View Issue' page

This pages lists the extension points for modules on the 'View issue' page in JIRA.

## Issue actions locations

The `jira.issue.tools` and `operations-*` locations together define the actions (share, export, more) on the top left of the 'View Issue' page.

#### Module type
`webSection` + `webItem`

#### Screenshot
<img src="../images/jdev-jiraissuetoolsops.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "jira.issue.tools",
            "name": {
                "value": "Example add-on name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "jira.issue.tools/operations-operations",
            "name": {
                "value": "Example add-on link"
            },
            "url": "/example-section-link
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: Set the location as follows:
    - For the webSection, set the location to `jira.issue.tools`, which is the **more** (...) menu on top right of 'View Issue' page.
    - For the webItem, set the location to `jira.issue.tools` + one of the following: `/operations-top-level`, `/operations-work`, `/operations-attachments`, `/operations-voteswatchers`, `/operations-subtasks`, `/operations-operations`, `/operations-delete`. The screenshot above shows where these locations are in the menu.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The target URL for the menu item.

----

## Right-side of the 'View Issue' page location

Defines web panels for the right hand side of the 'View Issue' page, as shown in the screenshot below.

#### Module type
`webPanel`

#### Screenshot
<img src="../images/jdev-viewissueright-location.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "webPanels": [
        {
            "key": "example-panel",
            "location": "atl.jira.view.issue.right.context",
            "name": {
                "value": "Example panel"
            },
            "url": "/my-example-panel"            
        }
    ]   
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: Set the location to `atl.jira.view.issue.right.context`.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The target URL for the navigation bar item.

  [i18n property]: /cloud/jira/platform/connect/modules/i18n-property
  [additional context]: /cloud/jira/platform/context-parameters
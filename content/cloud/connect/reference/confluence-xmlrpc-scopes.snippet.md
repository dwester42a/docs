# Confluence XML-RPC scopes
For more information about the Confluence XML-RPC APIs, please consult [the documentation on developer.atlassian.com](https://developer.atlassian.com/display/CONFDEV/Confluence+XML-RPC+and+SOAP+APIs).

The required scope for your add-on depends on the methods that your add-on invokes. 
The table below shows the required scope for every RPC method call.